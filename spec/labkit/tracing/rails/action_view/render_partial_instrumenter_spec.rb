# frozen_string_literal: true

require "spec_helper"
require_relative "../../../../support/tracing/shared_examples"

describe Labkit::Tracing::Rails::ActionView::RenderPartialInstrumenter do
  using RSpec::Parameterized::TableSyntax

  where(:identifier, :exception) do
    nil | nil
    "" | nil
    "show.haml" | nil
    nil | StandardError.new
  end

  with_them do
    it_behaves_like "a tracing instrumenter" do
      let(:expected_span_name) { "render_partial" }
      let(:payload) { { exception: exception, identifier: identifier } }
      let(:expected_tags) do
        { "component" => "ActionView", "template.id" => identifier }
      end
    end
  end

  describe "#span_name" do
    context "when the template identifier is nil do" do
      before do
        allow(Labkit::Tracing::Rails::ActionView).to receive(:template_identifier).and_return(nil)
      end

      it "returns plain span name" do
        expect(
          described_class.new.span_name(
            identifier: "/Users/adam/projects/notifications/app/views/posts/_form.html.erb",
          )
        ).to eql("render_partial")
      end
    end

    context "when a template identifier is returned do" do
      before do
        allow(Labkit::Tracing::Rails::ActionView).to receive(:template_identifier).and_return(
          "app/views/hello.html.erb"
        )
      end

      it "returns plain span name" do
        expect(
          described_class.new.span_name(
            identifier: "/Users/adam/projects/notifications/app/views/posts/_form.html.erb",
          )
        ).to eql("render_partial:app/views/hello.html.erb")
      end
    end
  end
end
