# frozen_string_literal: true

describe Labkit::Tracing do
  using RSpec::Parameterized::TableSyntax

  describe ".enabled?" do
    where(:connection_string, :enabled_state) do
      nil | false
      "" | false
      "opentracing://jaeger" | true
    end

    with_them do
      it "should return the correct state for .enabled?" do
        expect(described_class).to receive(:connection_string).and_return(connection_string)

        expect(described_class.enabled?).to eq(enabled_state)
      end
    end
  end

  describe ".tracing_url_enabled?" do
    using RSpec::Parameterized::TableSyntax

    where(:enabled?, :tracing_url_template, :tracing_url_enabled_state) do
      false | nil | false
      false | "" | false
      false | "http://localhost" | false
      true | nil | false
      true | "" | false
      true | "http://localhost" | true
    end

    with_them do
      it "should return the correct state for .tracing_url_enabled?" do
        expect(described_class).to receive(:enabled?).and_return(enabled?)
        allow(described_class).to receive(:tracing_url_template).and_return(tracing_url_template)

        expect(described_class.tracing_url_enabled?).to eq(tracing_url_enabled_state)
      end
    end
  end

  describe ".tracing_url" do
    using RSpec::Parameterized::TableSyntax

    where(:tracing_url_enabled?, :tracing_url_template, :correlation_id, :service_name, :tracing_url) do
      false | "https://localhost" | "123" | "web" | nil
      true | "https://localhost" | "123" | "web" | "https://localhost"
      true | "https://localhost?service={{ service }}" | "123" | "web" | "https://localhost?service=web"
      true | "https://localhost?c={{ correlation_id }}" | "123" | "web" | "https://localhost?c=123"
      true | "https://localhost?c={{ correlation_id }}&s={{ service }}" | "123" | "web" | "https://localhost?c=123&s=web"
      true | "https://localhost?c={{ correlation_id }}" | nil | "web" | "https://localhost?c="
    end

    with_them do
      it "should return the correct state for .tracing_url" do
        expect(described_class).to receive(:tracing_url_enabled?).and_return(tracing_url_enabled?)
        allow(described_class).to receive(:tracing_url_template).and_return(tracing_url_template)
        allow(Labkit::Correlation::CorrelationId).to receive(:current_id).and_return(correlation_id)

        expect(described_class.tracing_url(service_name)).to eq(tracing_url)
      end
    end
  end

  describe ".with_tracing" do
    it "should call the underlying method with original parameters" do
      params = { operation_name: "name", tags: { foo: :bar } }

      expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing)
                                                 .with(params)
                                                 .and_yield

      expect { |b| described_class.with_tracing(**params, &b) }.to yield_control
    end

    let(:fake_span) { double("OpenTracing span") }
    let(:fake_scope) { double("OpenTracing scope", span: fake_span) }

    it "should generate a backtrace when backtrace is enabled" do
      allow(OpenTracing.global_tracer).to receive(:start_active_span).and_return(fake_scope)
      allow(fake_scope).to receive(:close)
      allow(fake_span).to receive(:set_tag)

      expect(described_class).to receive(:stacktrace_operations).and_return(Set.new(["example"]))
      expect(fake_span).to receive(:log_kv).with(stack: be_a(String))

      params = { operation_name: "example: name", tags: { foo: :bar } }

      described_class.with_tracing(**params) { }
    end
  end

  describe ".stacktrace_operations" do
    before do
      Labkit::Tracing.instance_variable_set(:@stacktrace_operations, nil)
    end

    it "should return an empty set by default" do
      expect(Labkit::Tracing.stacktrace_operations).to eq(Set.new)
    end

    it "should parse comma-separated values from the env" do
      expect(ENV).to receive(:[]).with("GITLAB_TRACING_INCLUDE_STACKTRACE").and_return("redis, active_record")

      expect(Labkit::Tracing.stacktrace_operations).to eq(Set.new(%w[redis active_record]))
    end
  end
end
