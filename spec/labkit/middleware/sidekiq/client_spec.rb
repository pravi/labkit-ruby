# frozen_string_literal: true

require "sidekiq/testing"
require_relative "../../../support/sidekiq_middleware/shared_contexts"

describe Labkit::Middleware::Sidekiq::Client do
  let(:test_worker) do
    class DummyWorker
      include Sidekiq::Worker
    end
  end

  include_context "with sidekiq client middleware setup"

  shared_examples "calling client middleware" do |middleware|
    it "calls the #{middleware} client middleware" do
      fake_middleware = double("middleware")
      expect(middleware).to receive(:new).and_return(fake_middleware)
      expect(fake_middleware).to receive(:call)
                                   .with(test_worker.class, a_hash_including("args" => ["hello"]), anything, anything)

      test_worker.perform_async("hello")
    end
  end

  it_behaves_like "calling client middleware", Labkit::Middleware::Sidekiq::Context::Client

  context "when tracing is enabled" do
    before do
      described_class.instance_variable_set(:@chain, nil)
      allow(Labkit::Tracing).to receive(:enabled?).and_return(true)
    end

    it_behaves_like "calling client middleware", Labkit::Middleware::Sidekiq::Tracing::Client
  end
end
