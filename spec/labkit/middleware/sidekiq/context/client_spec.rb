# frozen_string_literal: true

require "sidekiq/testing"
require_relative "../../../../support/sidekiq_middleware/shared_contexts"

describe Labkit::Middleware::Sidekiq::Context::Client do
  let(:test_worker) do
    class DummyWorker
      include Sidekiq::Worker
    end
  end

  include_context "with sidekiq client middleware setup"

  it "adds the metadata to a job" do
    context = { "project" => "jane.doe/bookstore", "user" => "jane.doe", "caller_id" => "DummyWorker" }
    expected_metadata = context.transform_keys { |k| "#{Labkit::Context::LOG_KEY}.#{k}" }
    jid = nil

    Labkit::Context.with_context(context) do
      jid = test_worker.perform_async("do it")
    end

    job = find_job(jid)

    expect(job).to include(expected_metadata)
    expect(job.keys).to include(Labkit::Context::CORRELATION_ID_KEY)
  end

  def find_job(jid)
    test_worker.jobs.find { |j| j["jid"] == jid }
  end
end
