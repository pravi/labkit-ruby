# frozen_string_literal: true

describe Labkit::Middleware::Rack do
  let(:app) { double("app") }
  let(:env) { {} }

  describe "#call" do
    it "adds the correlation id from the request to the context" do
      fake_request = double("request")

      expect(ActionDispatch::Request).to receive(:new).with(env).and_return(fake_request)
      expect(fake_request).to receive(:request_id).and_return("the id")
      expect(Labkit::Context).to receive(:with_context).with(a_hash_including(Labkit::Context::CORRELATION_ID_KEY => "the id"))

      described_class.new(app).call(env)
    end

    it "calls the app" do
      expect(app).to receive(:call).with(env)

      described_class.new(app).call(env)
    end
  end
end
