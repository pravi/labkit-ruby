# frozen_string_literal: true

class TestRackServer
  def initialize(handler_proc)
    @handler_proc = handler_proc
    @started = false
  end

  def start
    @server = WEBrick::HTTPServer.new(
      Host: "127.0.0.1",
      Port: 9202,
      Logger: WEBrick::Log.new(nil, WEBrick::BasicLog::FATAL), AccessLog: [],
      StartCallback: -> { @started = true },
      StopCallback: -> { @started = false },
    )
    @server.mount "/api/v1/tests", Rack::Handler::WEBrick, @handler_proc
    @thread = Thread.new { @server.start }
    @thread.abort_on_exception = false
  end

  def stop
    @server&.shutdown
    sleep 0.01 until @started == false
    @thread&.kill
  end
end
